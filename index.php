<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="widtd=device-width initial-scale=1">

		<title>Access Modifiers and Encapsulation</title>
	</head>
	<body>

		<h1>Building</h1>

		<p>The name of the building is <?php echo $building->getBldgName(); ?>.</p>

		<p>The <?php echo $building->getBldgName(); ?> has <?php echo $building->getBldgFloors(); ?> floors.</p>		  
		<p>The <?php echo $building->getBldgName(); ?> is located at <?php echo $building->getBldgAddress(); ?>.</p>	
		<p><?php $building->setBldgName('Caswynn Complex'); ?></p>

		<p>The name of the condo has been changed to <?php echo $building->getBldgName(); ?>.</p> 

		<h1>Condominium</h1>

		<p>The name of the condominium is <?php echo $condominium->getCondoName(); ?>.</p>

		<p>The <?php echo $condominium->getCondoName(); ?> has <?php echo $condominium->getCondoFloors(); ?> floors.</p>		  

		<p>The <?php echo $condominium->getCondoName(); ?> is located at <?php echo $condominium->getCondoAddress(); ?>.</p>	

		<p><?php $condominium->setCondoName('Enzo Tower'); ?></p>

		<p>The name of the condominium has been changed to <?php echo $condominium->getCondoName(); ?>.</p> 
		
	</body>
</html>