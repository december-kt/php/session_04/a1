<?php 

	class Building {
		protected $name;
		protected $floors;
		protected $address;

		// Constructor
		public function __construct($name, $floors, $address) {
			$this->name = $name;
			$this->floors = $floors;
			$this->address = $address;
		}

		public function getBldgName() {
			return $this->name;
		}

		public function getBldgFloors() {
			return $this->floors;
		}

		public function getBldgAddress() {
			return $this->address;
		}

		public function setBldgName($name) {
			$this->name = $name;
		}
	}

	$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');

	class Condominium extends Building {
		public function getCondoName() {
			return $this->name;
		}

		public function getCondoFloors() {
			return $this->floors;
		}

		public function getCondoAddress() {
			return $this->address;
		}

		public function setCondoName($name) {
			$this->name = $name;
		}
	}

	$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines')
?>